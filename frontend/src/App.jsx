import React, {useEffect, useState} from 'react';
import TodoService from "./service/api";
import {Todo} from "./components/Todo";

export const App = () => {

    const [todos , setTodos ] =  useState([]);

    const [content , setContent] = useState("")

    useEffect(() => {
        const fetchTodos = async () => {
            try {
                const response = await TodoService.getAllTodos();
                const todos = response.data;

                setTodos(todos);
            }catch (e) {
                console.log(e)
            }
        }
        fetchTodos();
    },[])

    const addTodos = async (e) => {
        e.preventDefault();
        try {
            if (content.length > 3){
                const response = await TodoService.addTodos({todo:content})
                const newTodo = response.data;
                setContent("");
                setTodos(todos.concat(newTodo))
            }else {
                alert("Please Add More than 3 characters")
            }
        }catch (e) {
            console.error("Failed to create todo", e);
        }
    }

    return(
        <main className="container">
            <h1 className="title">Awesome Todos</h1>
            <form className="form" onSubmit={addTodos}>
                <input
                    type="text"
                    value={content}
                    onChange={(e) => setContent(e.target.value)}
                    placeholder="Enter a new Todo..."
                    className="form__input"
                    required
                />
                <button className="form__button" type="submit">Create Todo</button>
            </form>
            <div className="todos">
                {(todos).length > 0 && todos.map((todo) => (
                            <Todo key={todo._id} todo={todo} setTodos={setTodos}/>
                ))}
            </div>
        </main>
        // <div className="App">
        //     <h1>Hello World</h1>
        //     {
        //         todos.length > 0 ? (
        //             <ul>
        //                 {todos.map(todo => (
        //                     <li key={todo._id}>
        //                         {todo.todo} - {todo.status ? 'Done' : 'Pending'}
        //                     </li>
        //                 ))}
        //             </ul>
        //         ):(
        //             <p>No todos found</p>
        //
        //         )}
        // </div>
    )
};

