import axios from 'axios';

const API_URL = 'http://localhost:5000';

const axiosInstance = axios.create({
    baseURL: API_URL,
});

const TodoService = {
    getAllTodos:() => {
        return axiosInstance.get('api/todos')
    },
    addTodos:(data) => {
        return axiosInstance.post('api/todos',data)
    },
    updateTodo: (todoId, todoStatus) => {
        return axiosInstance.put(`api/todos/${todoId}`, {
            status: todoStatus,
        });
    },
    deleteTodo: (todoId) => {
        return axiosInstance.delete(`api/todos/${todoId}`);
    },

}
export default TodoService;