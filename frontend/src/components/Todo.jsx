import TodoService from "../service/api";

export const Todo = (props) => {
    const {todo , setTodos } = props;

    const updateTodo = async (todoId) => {
        const todoStatus = !todo.status; // Assuming you want to toggle the status
        try {
            const response = await TodoService.updateTodo(todoId, todoStatus);
            const json = response.data;
            if (json.acknowledged) {
                setTodos(currentTodos => {
                    return currentTodos.map((currentTodo) => {
                        if (currentTodo._id === todoId) {
                            return { ...currentTodo, status: todoStatus };
                        }
                        return currentTodo;
                    });
                });
            }
        } catch (error) {
            console.error("Failed to update todo:", error);
        }
    };

    const deleteTodo = async (todoId) => {
        try {
            const response = await TodoService.deleteTodo(todoId);
            const json = response.data;
            if (json.acknowledged) {
                setTodos(currentTodos => {
                    return currentTodos.filter((currentTodo) => currentTodo._id !== todoId);
                });
            }
        } catch (error) {
            console.error("Failed to delete todo:", error);
        }
    };

    return(
        <div className="todo">
            <p>{todo.todo}</p>
            <div className="mutations">
                <button
                    className="todo__status"
                    onClick={() => updateTodo(todo._id, todo.status)}
                >
                    {(todo.status) ? "☑" : "☐"}
                </button>
                <button
                    className="todo__delete"
                    onClick={() => deleteTodo(todo._id)}
                >
                    🗑️
                </button>
            </div>
        </div>
    )
}