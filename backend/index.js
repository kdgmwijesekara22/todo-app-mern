const express = require("express")
const cors = require("cors"); // Require the cors package

const app = express();
const path = require("path");

app.use(cors());
app.use(express.json())
app.use(express.static(path.join(__dirname, "build")));

app.get("/", (req, res) => {
    res.sendFile(path.join(__dirname, "build/index.html"));
})


const router = require("./route");
const {connectToMongoDB} = require("./db/dbcon");
app.use("/api",router)

const port = process.env.PORT || 5000;

const startServer = async () => {
    await connectToMongoDB();
    app.listen(port,() => {
        console.log(`Server is listening on port ${port}`)
    })
}

startServer();
