const express = require("express");
const {getConnectedClient} = require("./db/dbcon");
const {ObjectId} = require("mongodb");
const router = express.Router();

const getCollection = async () => {
    const client = await getConnectedClient();
    const collection = await client.db("todosDb").collection("todos");
    return collection;
}

router.get("/todos",async (req, res) => {
    try {
        console.log("Fetching todos...");
        const collection = await getCollection();
        const todos = await collection.find({}).toArray();
        console.log(`Fetched ${todos.length} todos.`);
        res.status(200).json(todos);
    }catch (error) {
        console.error("Error fetching todos:", error);
        res.status(500).json({ msg: "Failed to retrieve todos" });
    }

})

router.post("/todos",async (req,res) => {

    try {
        console.log("Attempting to post a new todo...");
        const collection = await getCollection();

        // let todo = req.body.todo;
        let {todo} = req.body;
        console.log("Received todo:", todo);

        if (!todo) {
            console.log("No todo found in the request body.");
            return res.status(400).json({msg:"ERROR OCCURRED NO TODO FOUND"});
        }
        todo = (typeof todo == 'string') ? todo: JSON.stringify(todo);
        console.log("Processed todo:", todo);

        const newTodo = await collection.insertOne({todo,status:false});

        console.log("New todo inserted:", newTodo);

        res.status(201).json({todo,status:false,_id:newTodo.insertId})
    }catch (error) {
        console.error("Failed to create todo:", error);
        res.status(500).json({ msg: "Failed to create todo" });
    }

})

router.delete("/todos/:id",async (req,res) => {
    try {
        const collection = await getCollection(); // Added await here

        const _id = new ObjectId(req.params.id);
        console.log("id==>",req.params.id)
        console.log("object id ===> ", _id)

        const deleteTodo = await collection.deleteOne({_id});

        if (deleteTodo.deletedCount === 0) {
            return res.status(404).json({ msg: "Todo not found" });
        }
        res.status(200).json(deleteTodo);

    }catch (error) {
        res.status(400).json({ msg: "Invalid todo ID" });
    }

});
router.put("/todos/:id",async (req,res) => {

    try {
        const collection = await getCollection();
        const _id = new ObjectId(req.params.id);
        const {status} = req.body;

        if(typeof status !== "boolean") {
            return res.status(400).json({msg:"invalid Status"})
        }
        const updateTodo = await collection.updateOne({_id},{$set:{status:!status}});

        if (updateTodo.matchedCount === 0) {
            return res.status(404).json({ msg: "Todo not found" });
        }
        res.status(200).json(updateTodo);
    }catch (error) {
        res.status(400).json({ msg: "Invalid todo ID" });
    }

})

module.exports = router;